<%

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.1.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Utility\Inflector;

$fields = collection($fields)
        ->filter(function($field) use ($schema) {
    return $schema->columnType($field) !== 'binary';
});

if (isset($modelObject) && $modelObject->behaviors()->has('Tree')) {
    $fields = $fields->reject(function ($field) {
        return $field === 'lft' || $field === 'rght';
    });
}


$titulo = $action == 'add' ? 'Create' : 'Edit';
%>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= __('<%= $titulo .' '.$singularHumanName %>') ?> <small>* required</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create($<%= $singularVar %>, ["class" => "form-horizontal form-label-left"]) ?>
                <?= $this->Flash->render() ?>

                <%
                foreach ($fields as $field) {
                    %>
                
                    <%
                    if (in_array($field, $primaryKey)) {
                        continue;
                    }
                    if (isset($keyFields[$field]) && !in_array($field, ['status'])) {
                        $fieldData = $schema->column($field);
                        if (!empty($fieldData['null'])) {
                            %>
                <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="<%= $field %>"><%= $field %> <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('<%= $field %>', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $<%= $keyFields[$field] %>, 'empty' => true]);?>
                                </div> 
                            </div> 
                            <%
                        } else {
                            %>
                <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="<%= $field %>"><%= $field %> <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('<%= $field %>', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $<%= $keyFields[$field] %>]);?>
                                </div> 
                            </div> 
                            <%
                        }
                        continue;
                    }
                    if (!in_array($field, ['created', 'modified', 'updated', 'last_update'])) {
                        $fieldData = $schema->column($field);
                        if (in_array($fieldData['type'], ['date', 'datetime', 'time']) && (!empty($fieldData['null']))) {
                            %>
                <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="<%= $field %>"><%= $field %> <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('<%= $field %>', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => true]); ?>

                                </div> 
                            </div>  <%
                        } else {
                            if (!in_array($field, ['status'])) {
                                %>
                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="<%= $field %>"><%= $field %> <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <?= $this->Form->input('<%= $field %>', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                                    </div> 
                                </div>  <%
                            }
                        }
                    }
                    %>
                       
                    <%
                }
                if (!empty($associations['BelongsToMany'])) {
                    foreach ($associations['BelongsToMany'] as $assocName => $assocData) {
                        %>
                        <?= $this->Form->input('<%= $assocData['property'] %>._ids', ['options' => $<%= $assocData['variable'] %>]);?>
                        <%
                    }
                }

                $button = $action == 'add' ? 'Create' : 'Edit';
                %>

                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success"><%=$button %></button>
                        <?= $this->Html->link(__('Voltar'), $backlink, ['class' => "btn btn-primary"]) ?>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
