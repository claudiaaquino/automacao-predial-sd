<%

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.1.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Utility\Inflector;

$fields = collection($fields)
        ->filter(function($field) use ($schema) {
    return !in_array($schema->columnType($field), ['binary', 'text']);
});

if (isset($modelObject) && $modelObject->behaviors()->has('Tree')) {
    $fields = $fields->reject(function ($field) {
        return $field === 'lft' || $field === 'rght';
    });
}

if (!empty($indexColumns)) {
    $fields = $fields->take($indexColumns);
}
%>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2><?= __('<%= $pluralHumanName %>') ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link(__('  Criar novo'), ['action' => 'add'], ['class' => "btn btn-dark fa fa-file"]) ?></li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Flash->render() ?>
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th>
                                    <input type="checkbox" id="check-all" class="flat">
                                </th>
                                <% foreach ($fields as $field): %>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('<%= $field %>') ?></th>
                                <% endforeach; %>
                                
                                <th class="bulk-actions" colspan="7">
                                    <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                </th>
                                <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $cor = 'even';
                            foreach ($<%= $pluralVar %> as $<%= $singularVar %>){ ?>                                
                                <tr class="<?= $cor ?> pointer">
                                     <% $pk = '$' . $singularVar . '->id';%>
                                    
                                    <td class="a-center ">
                                        <input type="checkbox" class="flat" name="table_records" value="<?=<%= $pk %>?>">
                                    </td>
                                    
                                     <%
                                    foreach ($fields as $field) {
                                        $isKey = false;
                                        if (!empty($associations['BelongsTo'])) {
                                            foreach ($associations['BelongsTo'] as $alias => $details) {
                                                if ($field === $details['foreignKey']) {
                                                    $isKey = true;
                                                    %>
                                                     <td><?= $<%= $singularVar %>->has('<%= $details['property'] %>') ? $this->Html->link($<%= $singularVar %>-><%= $details['property'] %>-><%= $details['displayField'] %>, ['controller' => '<%= $details['controller'] %>', 'action' => 'view', $<%= $singularVar %>-><%= $details['property'] %>-><%= $details['primaryKey'][0] %>]) : '' ?></td>
                                                    <%
                                                    break;
                                                }
                                            }
                                        }
                                        if ($isKey !== true) {
                                            if (!in_array($schema->columnType($field), ['integer', 'biginteger', 'decimal', 'float'])) {
                                                %>
                                                 <td><?= h($<%= $singularVar %>-><%= $field %>) ?></td>
                                                <%
                                            } else {
                                                %>
                                                  <td><?= $this->Number->format($<%= $singularVar %>-><%= $field %>) ?></td>
                                                <%
                                            }
                                        }
                                    }

                                    
                                    %>
                                    
                                    <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('View'), ['action' => 'view', <%= $pk %>], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Edit'), ['action' => 'edit', <%= $pk %>], [ 'class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', <%= $pk %>], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja remover esse registro?', <%= $pk %>)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            }
                            ?>
                        </tbody>                       
                    </table>
                    <div class="paginator">
                        <ul class="pagination">
                            <?= $this->Paginator->prev('< ' . __('previous')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('next') . ' >') ?>
                        </ul>
                        <?= $this->Paginator->counter() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
