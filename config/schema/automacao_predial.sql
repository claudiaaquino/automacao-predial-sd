/*
Navicat MySQL Data Transfer

Source Server         : Triade Local
Source Server Version : 50711
Source Host           : localhost:3306
Source Database       : automacao_predial

Target Server Type    : MYSQL
Target Server Version : 50711
File Encoding         : 65001

Date: 2019-06-27 21:31:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for devices
-- ----------------------------
DROP TABLE IF EXISTS `devices`;
CREATE TABLE `devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL,
  `state` tinyint(1) DEFAULT NULL,
  `no_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `NOID` (`no_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of devices
-- ----------------------------
INSERT INTO `devices` VALUES ('1', 'Lâmpada 1', '0', '1');
INSERT INTO `devices` VALUES ('2', 'Lâmpada 2', '0', '2');

-- ----------------------------
-- Table structure for logs
-- ----------------------------
DROP TABLE IF EXISTS `logs`;
CREATE TABLE `logs` (
  `time` varchar(100) NOT NULL,
  `temp` varchar(100) DEFAULT NULL,
  `hall` varchar(100) DEFAULT NULL,
  `device_id` int(11) NOT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`time`),
  KEY `DEVICEID` (`device_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of logs
-- ----------------------------

-- ----------------------------
-- Table structure for nos
-- ----------------------------
DROP TABLE IF EXISTS `nos`;
CREATE TABLE `nos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL,
  `flagfail` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of nos
-- ----------------------------
INSERT INTO `nos` VALUES ('1', 'Quarto', '0');
INSERT INTO `nos` VALUES ('2', 'Sala', '0');

-- ----------------------------
-- Table structure for rules
-- ----------------------------
DROP TABLE IF EXISTS `rules`;
CREATE TABLE `rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `conditions` varchar(100) DEFAULT NULL,
  `results` varchar(100) DEFAULT NULL,
  `device_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `DEVICEID` (`device_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rules
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `username` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `lastlogin` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('7', 'Claudia Aquino', 'claudiaaquino', '$2y$10$HA1x.HXu8fxIT.ror6IO/.zj.3gVAASWHGxYhe2VAPkgoEtQuf21.', 'claudia@email.com', '2019-06-27 09:55:53', '2019-06-25 21:26:36', '2019-06-27 09:55:53', '1');
