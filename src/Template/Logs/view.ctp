<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i><?= __('Logs') ?> - <?= h($log->time) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">

                        <p class="title"><?= __('Dispositivo') ?></p>
                        <p><?= $log->has('device') ? $this->Html->link($log->device->name, ['controller' => 'Devices',
                            'action' => 'view', $log->device->id]) : 'Não Informado' ?></p>

                        <p class="title"><?= __('Time') ?></p>
                        <p><?= $log->time ? $log->time : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Temperature') ?></p>
                        <p><?= $log->temp ? $log->temp : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Hall') ?></p>
                        <p><?= $log->hall ? $log->hall : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Created') ?></p>
                        <p><?= $log->created ? $log->created : 'Não Informado'; ?></p>

                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                        <?= $this->Html->link("Editar", ['action' => 'edit', $log->time], ['class' => "btn
                        btn-primary"]) ?>
                        <?= $this->Form->postLink("Deletar", ['action' => 'delete', $log->time], ['class' => "btn
                        btn-danger", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $log->time)])
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


