<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= __('Logs') ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Flash->render() ?>
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                        <tr class="headings">
                            <th>
                                <input type="checkbox" id="check-all" class="flat">
                            </th>
                            <th scope="col" class="column-title"><?= $this->Paginator->sort('time') ?></th>
                            <th scope="col" class="column-title"><?= $this->Paginator->sort('temp') ?></th>
                            <th scope="col" class="column-title"><?= $this->Paginator->sort('hall') ?></th>
                            <th scope="col" class="column-title"><?= $this->Paginator->sort('device_id') ?></th>
                            <th scope="col" class="column-title"><?= $this->Paginator->sort('created') ?></th>

                            <th class="bulk-actions" colspan="7">
                                <a class="antoo" style="color:#fff; font-weight:500;">( <span
                                    class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                            </th>
                            <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            $cor = 'even';
                            foreach ($logs as $log){ ?>
                        <tr class="<?= $cor ?> pointer">

                            <td class="a-center ">
                                <input type="checkbox" class="flat" name="table_records" value="<?=$log->id?>">
                            </td>

                            <td><?= h($log->time) ?></td>
                            <td><?= h($log->temp) ?></td>
                            <td><?= h($log->hall) ?></td>
                            <td><?= $log->has('device') ? $this->Html->link($log->device->name, ['controller' =>
                                'Devices', 'action' => 'view', $log->device->id]) : '' ?>
                            </td>
                            <td><?= h($log->created) ?></td>

                            <td class=" last">
                                <div class="btn-group">
                                    <?= $this->Html->link(__('View'), ['action' => 'view', $log->id], ['class' => "btn
                                    btn-primary btn-xs"]) ?>
                                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $log->id], [ 'class' => "btn
                                    btn-info btn-xs"]) ?>
                                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $log->id], ['class'
                                    => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja remover
                                    esse registro?', $log->id)]) ?>
                                </div>
                            </td>
                        </tr>
                        <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            }
                            ?>
                        </tbody>
                    </table>
                    <div class="paginator">
                        <ul class="pagination">
                            <?= $this->Paginator->prev('< ' . __('previous')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('next') . ' >') ?>
                        </ul>
                        <?= $this->Paginator->counter() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
