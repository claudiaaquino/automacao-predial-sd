<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i><?= __('Rules') ?> - <?= h($rule->id) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">

                        <p class="title"><?= __('Id') ?></p>
                        <p><?= $rule->id ? $this->Number->format($rule->id) : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Condições') ?></p>
                        <p><?= $rule->conditions ? $rule->conditions : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Resultados') ?></p>
                        <p><?= $rule->results ? $rule->results : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Dispositivo') ?></p>
                        <p><?= $rule->has('device') ? $this->Html->link($rule->device->name, ['controller' => 'Devices',
                            'action' => 'view', $rule->device->id]) : 'Não Informado' ?></p>

                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                        <?= $this->Html->link("Editar", ['action' => 'edit', $rule->id], ['class' => "btn btn-primary"])
                        ?>
                        <?= $this->Form->postLink("Deletar", ['action' => 'delete', $rule->id], ['class' => "btn
                        btn-danger", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $rule->id)])
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


