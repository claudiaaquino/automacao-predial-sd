<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= __('Rules') ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link(__(' Criar novo'), ['action' => 'add'], ['class' => "btn btn-dark fa
                        fa-file"]) ?>
                    </li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Flash->render() ?>
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                        <tr class="headings">
                            <th>
                                <input type="checkbox" id="check-all" class="flat">
                            </th>
                            <th scope="col" class="column-title"><?= $this->Paginator->sort('id') ?></th>
                            <th scope="col" class="column-title"><?= $this->Paginator->sort('conditions') ?></th>
                            <th scope="col" class="column-title"><?= $this->Paginator->sort('results') ?></th>
                            <th scope="col" class="column-title"><?= $this->Paginator->sort('device_id') ?></th>

                            <th class="bulk-actions" colspan="7">
                                <a class="antoo" style="color:#fff; font-weight:500;">( <span
                                    class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                            </th>
                            <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            $cor = 'even';
                            foreach ($rules as $rule){ ?>
                        <tr class="<?= $cor ?> pointer">

                            <td class="a-center ">
                                <input type="checkbox" class="flat" name="table_records" value="<?=$rule->id?>">
                            </td>

                            <td><?= $this->Number->format($rule->id) ?></td>
                            <td><?= h($rule->conditions) ?></td>
                            <td><?= h($rule->results) ?></td>
                            <td><?= $rule->has('device') ? $this->Html->link($rule->device->name, ['controller' =>
                                'Devices', 'action' => 'view', $rule->device->id]) : '' ?>
                            </td>

                            <td class=" last">
                                <div class="btn-group">
                                    <?= $this->Html->link(__('View'), ['action' => 'view', $rule->id], ['class' => "btn
                                    btn-primary btn-xs"]) ?>
                                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $rule->id], [ 'class' => "btn
                                    btn-info btn-xs"]) ?>
                                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $rule->id], ['class'
                                    => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja remover
                                    esse registro?', $rule->id)]) ?>
                                </div>
                            </td>
                        </tr>
                        <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            }
                            ?>
                        </tbody>
                    </table>
                    <div class="paginator">
                        <ul class="pagination">
                            <?= $this->Paginator->prev('< ' . __('previous')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('next') . ' >') ?>
                        </ul>
                        <?= $this->Paginator->counter() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
