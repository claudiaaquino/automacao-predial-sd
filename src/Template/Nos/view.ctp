<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i><?= __('Nos') ?> - <?= h($no->name) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">


                        <p class="title"><?= __('Id') ?></p>
                        <p><?= $no->id ? $this->Number->format($no->id) : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Name') ?></p>
                        <p><?= $no->name ? $no->name : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Status') ?></p>
                        <p><?= $no->flagfail ? __('Ativo') : __('Fail'); ?></p>

                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                        <?= $this->Html->link("Editar", ['action' => 'edit', $no->id], ['class' => "btn btn-primary"])
                        ?>
                        <?= $this->Form->postLink("Deletar", ['action' => 'delete', $no->id], ['class' => "btn
                        btn-danger", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $no->id)]) ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if (!empty($no->devices)): ?>
        <div class="x_panel">
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Devices Vínculados </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                        <tr class="headings">
                            <th scope="col" class="column-title"><?= __('Id') ?></th>
                            <th scope="col" class="column-title"><?= __('Name') ?></th>
                            <th scope="col" class="column-title"><?= __('State') ?></th>
                            <th scope="col" class="column-title"><?= __('No Id') ?></th>
                            <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                                $cor = 'even';
                                foreach ($no->devices as $devices):
                        ?>
                        <tr class="<?= $cor ?> pointer">
                            <td><?= h($devices->id) ?></td>
                            <td><?= h($devices->name) ?></td>
                            <td><?= h($devices->state) ?></td>
                            <td><?= h($devices->no_id) ?></td>

                            <td class=" last">
                                <div class="btn-group">
                                    <?= $this->Html->link(__('Visualizar'), ['controller' => 'Devices', 'action' =>
                                    'view', $devices->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                    <?= $this->Html->link(__('Editar'), ['controller' => 'Devices', 'action' => 'edit',
                                    $devices->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                    <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Devices', 'action' =>
                                    'delete', $devices->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você
                                    tem certeza que deseja deletar esse registro?', $devices->id)]) ?>
                                </div>
                            </td>
                        </tr>
                        <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            endforeach;
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>


