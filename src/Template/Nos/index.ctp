<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= __('Nos') ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Flash->render() ?>
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                        <tr class="headings">
                            <th>
                                <input type="checkbox" id="check-all" class="flat">
                            </th>
                            <th scope="col" class="column-title"><?= $this->Paginator->sort('id') ?></th>
                            <th scope="col" class="column-title"><?= $this->Paginator->sort('name') ?></th>
                            <th scope="col" class="column-title"><?= $this->Paginator->sort('flagfail') ?></th>

                            <th class="bulk-actions" colspan="7">
                                <a class="antoo" style="color:#fff; font-weight:500;">( <span
                                    class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                            </th>
                            <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            $cor = 'even';
                            foreach ($nos as $no){ ?>
                        <tr class="<?= $cor ?> pointer">

                            <td class="a-center ">
                                <input type="checkbox" class="flat" name="table_records" value="<?=$no->id?>">
                            </td>

                            <td><?= $this->Number->format($no->id) ?></td>
                            <td><?= h($no->name) ?></td>
                            <td><?= h($no->flagfail) ?></td>

                            <td class=" last">
                                <div class="btn-group">
                                    <?= $this->Html->link(__('View'), ['action' => 'view', $no->id], ['class' => "btn
                                    btn-primary btn-xs"]) ?>
                                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $no->id], [ 'class' => "btn
                                    btn-info btn-xs"]) ?>
                                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $no->id], ['class' =>
                                    "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja remover esse
                                    registro?', $no->id)]) ?>
                                </div>
                            </td>
                        </tr>
                        <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            }
                            ?>
                        </tbody>
                    </table>
                    <div class="paginator">
                        <ul class="pagination">
                            <?= $this->Paginator->prev('< ' . __('previous')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('next') . ' >') ?>
                        </ul>
                        <?= $this->Paginator->counter() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
