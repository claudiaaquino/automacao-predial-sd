<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= __('Dispositivos') ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link(__(' Criar novo'), ['action' => 'add'], ['class' => "btn btn-dark fa
                        fa-file"]) ?>
                    </li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Flash->render() ?>
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                        <tr class="headings">
                            <th>
                                <input type="checkbox" id="check-all" class="flat">
                            </th>
                            <th scope="col" class="column-title"><?= $this->Paginator->sort('id') ?></th>
                            <th scope="col" class="column-title"><?= $this->Paginator->sort('name') ?></th>
                            <th scope="col" class="column-title"><?= $this->Paginator->sort('state') ?></th>
                            <th scope="col" class="column-title"><?= $this->Paginator->sort('no_id') ?></th>

                            <th class="bulk-actions" colspan="7">
                                <a class="antoo" style="color:#fff; font-weight:500;">( <span
                                    class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                            </th>
                            <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            $cor = 'even';
                            foreach ($devices as $device){ ?>
                        <tr class="<?= $cor ?> pointer">

                            <td class="a-center ">
                                <input type="checkbox" class="flat" name="table_records" value="<?=$device->id?>">
                            </td>

                            <td><?= $this->Number->format($device->id) ?></td>
                            <td><?= h($device->name) ?></td>
                            <td><?= $device->state ? 'Ligado' : 'Desligado'?></td>
                            <td><?= $device->has('no') ? $this->Html->link($device->no->name, ['controller' => 'Nos',
                                'action' => 'view', $device->no->id]) : '' ?>
                            </td>

                            <td class=" last">
                                <div class="btn-group">
                                    <?= $this->Html->link(__('View'), ['action' => 'view', $device->id], ['class' =>
                                    "btn btn-primary btn-xs"]) ?>
                                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $device->id], [ 'class' =>
                                    "btn btn-info btn-xs"]) ?>
                                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $device->id],
                                    ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja
                                    remover esse registro?', $device->id)]) ?>
                                </div>
                            </td>
                        </tr>
                        <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            }
                            ?>
                        </tbody>
                    </table>
                    <div class="paginator">
                        <ul class="pagination">
                            <?= $this->Paginator->prev('< ' . __('previous')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('next') . ' >') ?>
                        </ul>
                        <?= $this->Paginator->counter() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
