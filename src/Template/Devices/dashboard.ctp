<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_content">
            <?= $this->Flash->render() ?>
            <div class="row" id="realtime-box"></div>
        </div>
    </div>
    <?= $this->Form->input('urlroot', ["type" => "hidden", "value" => $this->request->webroot, 'label' => false]); ?>
</div>
<?= $this->Html->script('/js/mqttws31.js'); ?>
<script>
    var options = null;
    var devices = null;

    var client = null;
    var led_is_on = {};
    var hostname = "192.168.43.140";
    var port = "9001";
    var clientId = "System";

    $(document).ready(function () {
        connect();
        getAllDevices();
        setInterval(requestLedStatus, 10000);
    });

    function getAllDevices() {
        $.ajax({
            url: $('#urlroot').val() + 'devices/all/',
            type: 'GET',
            success: function (data) {
                devices = data.devices;
                displayAllDevices(devices);
                subscribeAllDevices(devices);
            }
        });
    }

    function subscribeAllDevices(devices) {
        $.each(devices, function (id, nome) {
            client.subscribe('ESP' + id + '/TEMPERATURA', options);
            client.subscribe('ESP' + id + '/HALL', options);
            client.subscribe('ESP' + id + '/LED/STATUS', options);
        });
    }

    function displayAllDevices(devices) {
        $.each(devices, function (id, nome) {
            $.ajax({
                url: $('#urlroot').val() + 'devices/realtime/' + id,
                type: 'GET',
                success: function (data) {
                    $('#realtime-box').append(data);
                }
            });
        });
    }

    function connect() {
        // Set up the client
        client = new Paho.MQTT.Client(hostname, Number(port), clientId);
        console.info('Connecting to Server: Hostname: ', hostname,
            '. Port: ', port, '. Client ID: ', clientId);

        // set callback handlers
        client.onConnectionLost = onConnectionLost;
        client.onMessageArrived = onMessageArrived;

        // see client class docs for all the options
        options = {
            onSuccess: onConnect, // after connected, subscribes
            onFailure: onFail     // useful for logging / debugging
        };
        // connect the client
        client.connect(options);
        console.info('Connecting...');
    }


    function onConnect(context) {
        console.log("Client Connected");
        // And subscribe to our topics	-- both with the same callback function
        options = {
            qos: 0, onSuccess: function (context) {
                console.log("subscribed");
            }
        }
    }

    function onFail(context) {
        console.log("Failed to connect");
    }

    function onConnectionLost(responseObject) {
        if (responseObject.errorCode !== 0) {
            console.log("Connection Lost: " + responseObject.errorMessage);
        }
    }

    function onMessageArrived(message) {
        console.log(message.destinationName, message.payloadString);
        var device = message.destinationName.substring(3, 4);

        // Update element depending on which topic's data came in
        if (message.destinationName.includes('EMPERATUR')) {
            document.getElementById(message.destinationName.replace('/', '_')).innerHTML = message.payloadString;
        } else if (message.destinationName.includes('HALL')) {
            document.getElementById(message.destinationName.replace('/', '_')).innerHTML = message.payloadString;
        } else if (message.destinationName.includes('LED/STATUS')) {
            if (message.payloadString == "on" || message.payloadString == "o") {
                document.getElementById(message.destinationName.split('/').join("_")).innerHTML = "ON";
                led_is_on[device] = true;
            } else {
                document.getElementById(message.destinationName.split('/').join("_")).innerHTML = "OFF";
                led_is_on[device] = false;
            }
        }
    }


    function requestLedStatus() {
        $.each(devices, function (id, nome) {
            message = new Paho.MQTT.Message("");
            message.destinationName = "ESP" + id + "/LED/REQUESTSTATUS";
            message.retained = true;
            client.send(message);
            console.info('requesting led status from ESP' + id);
        });
    }


    function led_toggle(device_id) {
        if (led_is_on[device_id]) {
            var payload = "off";
            led_is_on[device_id] = false;
        } else {
            var payload = "on";
            led_is_on[device_id] = true;
        }

        message = new Paho.MQTT.Message(payload);
        message.destinationName = "ESP" + device_id + "/LED";
        message.retained = true;
        client.send(message);
        console.info('sending: ', payload);
    }


</script>
