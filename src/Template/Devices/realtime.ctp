<div class="col-md-3 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>ESP <?= $device->name ?>
                <small>Nó <?= $device->no->name ?></small>
            </h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <article class="media event">
                <a class="pull-left date">
                    <p class="month">TempºC</p>
                    <p class="day" id="ESP<?= $device->id ?>_TEMPERATURA"> 0</p>
                </a>
                <a class="pull-left date">
                    <p class="month">HALL</p>
                    <p class="day" id="ESP<?= $device->id ?>_HALL"> 0</p>
                </a>
                <a class="pull-left date" href="javascript:void(0)" onclick="led_toggle(<?= $device->id ?>);">
                    <p class="month"><i class="fa fa-lightbulb-o"></i></p>
                    <p class="day" id="ESP<?= $device->id ?>_LED_STATUS"> OFF</p>
                </a>
            </article>
        </div>
    </div>
</div>
