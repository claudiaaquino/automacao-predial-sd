<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i><?= __('Dispositivos') ?> - <?= h($device->name) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Name') ?></p>
                        <p><?= $device->name ? $device->name : 'Não Informado'; ?></p>

                        <p class="title"><?= __('No') ?></p>
                        <p><?= $device->has('no') ? $this->Html->link($device->no->name, ['controller' => 'Nos',
                            'action' => 'view', $device->no->id]) : 'Não Informado' ?></p>


                        <p class="title"><?= __('Id') ?></p>
                        <p><?= $device->id ? $this->Number->format($device->id) : 'Não Informado'; ?></p>


                        <p class="title"><?= __('State') ?></p>
                        <p><?= $device->state ? __('Ativo') : __('Desativado'); ?></p>

                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                        <?= $this->Html->link("Editar", ['action' => 'edit', $device->id], ['class' => "btn
                        btn-primary"]) ?>
                        <?= $this->Form->postLink("Deletar", ['action' => 'delete', $device->id], ['class' => "btn
                        btn-danger", 'confirm' => __('Você tem certeza que deseja deletar esse registro?',
                        $device->id)]) ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if (!empty($device->rules)): ?>
        <div class="x_panel">
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Rules Vínculados </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                        <tr class="headings">
                            <th scope="col" class="column-title"><?= __('Id') ?></th>
                            <th scope="col" class="column-title"><?= __('Conditions') ?></th>
                            <th scope="col" class="column-title"><?= __('Results') ?></th>
                            <th scope="col" class="column-title"><?= __('Dispositivo Id') ?></th>
                            <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                                $cor = 'even';
                                foreach ($device->rules as $rules):
                        ?>
                        <tr class="<?= $cor ?> pointer">
                            <td><?= h($rules->id) ?></td>
                            <td><?= h($rules->conditions) ?></td>
                            <td><?= h($rules->results) ?></td>
                            <td><?= h($rules->device_id) ?></td>

                            <td class=" last">
                                <div class="btn-group">
                                    <?= $this->Html->link(__('Visualizar'), ['controller' => 'Rules', 'action' =>
                                    'view', $rules->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                    <?= $this->Html->link(__('Editar'), ['controller' => 'Rules', 'action' => 'edit',
                                    $rules->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                    <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Rules', 'action' =>
                                    'delete', $rules->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você
                                    tem certeza que deseja deletar esse registro?', $rules->id)]) ?>
                                </div>
                            </td>
                        </tr>
                        <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            endforeach;
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <?php if (!empty($device->logs)): ?>
        <div class="x_panel">
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Logs Vínculados </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                        <tr class="headings">
                            <th scope="col" class="column-title"><?= __('Time') ?></th>
                            <th scope="col" class="column-title"><?= __('Temp') ?></th>
                            <th scope="col" class="column-title"><?= __('Hall') ?></th>
                            <th scope="col" class="column-title"><?= __('Dispositivo ID') ?></th>
                            <th scope="col" class="column-title"><?= __('Created') ?></th>
                            <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                                $cor = 'even';
                                foreach ($device->logs as $logs):
                        ?>
                        <tr class="<?= $cor ?> pointer">
                            <td><?= h($logs->time) ?></td>
                            <td><?= h($logs->temp) ?></td>
                            <td><?= h($logs->hall) ?></td>
                            <td><?= h($logs->device_id) ?></td>
                            <td><?= h($logs->created) ?></td>

                            <td class=" last">
                                <div class="btn-group">
                                    <?= $this->Html->link(__('Visualizar'), ['controller' => 'Logs', 'action' => 'view',
                                    $logs->time], ['class' => "btn btn-primary btn-xs"]) ?>
                                    <?= $this->Html->link(__('Editar'), ['controller' => 'Logs', 'action' => 'edit',
                                    $logs->time], [ 'class' => "btn btn-info btn-xs"]) ?>
                                    <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Logs', 'action' =>
                                    'delete', $logs->time], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você
                                    tem certeza que deseja deletar esse registro?', $logs->time)]) ?>
                                </div>
                            </td>
                        </tr>
                        <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            endforeach;
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php endif; ?>

    </div>
</div>


