<!DOCTYPE html>
<html lang="pt">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Automação Predial</title>
        <?= $this->Html->meta('favicon.ico', '/favicon.ico', ['type' => 'icon']) ?>

        <?= $this->Html->css('/vendors/bootstrap/dist/css/bootstrap.min.css') ?>
        <?= $this->Html->css('/vendors/font-awesome/css/font-awesome.min.css') ?>
        <?= $this->Html->css('/vendors/animate.css/animate.min.css') ?>
        <?= $this->Html->css('custom.min.css') ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>    

    </head>


    <body class="login">
        <div>
            <a class="hiddenanchor" id="signin"></a>
            <a class="hiddenanchor" id="signup"></a>

            <div class="login_wrapper">
                <div class="animate form login_form">
                    <section class="login_content">
                        <?= $this->Form->create() ?>

                        <h1>Automação Predial</h1>
                        <p><?= $this->Flash->render() ?></p>
                        <p><?= $this->Flash->render('auth') ?></p>
                        <div>
                            <?= $this->Form->input('username', ['label' => false, "class" => "form-control", "placeholder" => "Username"]) ?>
                            <?= $this->Form->input('password', ['label' => false, "class" => "form-control", "placeholder" => "Password"]) ?>
                        </div>
                        <div>
                            <?= $this->Form->button(__('Login'), ["class" => "btn btn-default submit"]); ?>
                        </div>

                        <div class="clearfix"></div>

                        <?= $this->Form->end() ?>
                    </section>
                </div>
            </div>
        </div>
    </body>
</html>
