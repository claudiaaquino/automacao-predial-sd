
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2><?= __('Berufstätige') ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Flash->render() ?>
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th>
                                    <input type="checkbox" id="check-all" class="flat">
                                </th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('name') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('email') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('zipcode') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('city') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('cellphone') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('created') ?></th>

                                <th class="bulk-actions" colspan="7">
                                    <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                </th>
                                <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $cor = 'even';
                            foreach ($users as $user) {
                                ?>                                
                                <tr class="<?= $cor ?> pointer">

                                    <td class="a-center ">
                                        <input type="checkbox" class="flat" name="table_records" value="<?= $user->id ?>">
                                    </td>

                                    <td><?= h($user->name) ?></td>
                                    <td><?= h($user->email) ?></td>
                                    <td><?= h($user->zipcode) ?></td>
                                    <td><?= h($user->city) ?></td>
                                    <td><?= h($user->cellphone) ?></td>
                                    <td><?= h($user->created) ?></td>

                                    <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('View'), ['action' => 'view', $user->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            }
                            ?>
                        </tbody>                       
                    </table>
                    <div class="paginator">
                        <ul class="pagination">
                            <?= $this->Paginator->prev('< ' . __('previous')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('next') . ' >') ?>
                        </ul>
                        <?= $this->Paginator->counter() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
