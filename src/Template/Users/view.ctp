<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Profile</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <?= $this->Flash->render() ?>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">

                        <p class="title"><?= __('Name') ?></p>
                        <p><?= $user->name ? $user->name : 'keine Information'; ?></p>

                        <p class="title"><?= __('Username') ?></p>
                        <p><?= $user->username ? $user->username : 'keine Information'; ?></p>


                        <p class="title"><?= __('Email') ?></p>
                        <p><?= $user->email ? $user->email : 'keine Information'; ?></p>

                        </div>
                </div>



                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Lastlogin') ?></p>
                        <p><?= $user->lastlogin ? $user->lastlogin : 'keine Information'; ?></p>


                        <p class="title"><?= __('Created') ?></p>
                        <p><?= $user->created ? $user->created : 'keine Information'; ?></p>


                        <p class="title"><?= __('Updated') ?></p>
                        <p><?= $user->updated ? $user->updated : 'keine Information'; ?></p>


                        <p class="title"><?= __('Status') ?></p>
                        <p><?= $user->status ? __('Aktiv') : __('Inaktiv'); ?></p>

                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("zurück", $backlink, ['class' => "btn btn-default"]) ?>
                        <?php if ($loggeduser == $user->id) { ?>
                            <?= $this->Html->link("ändern", ['action' => 'edit', $user->id], ['class' => "btn btn-primary"]) ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


