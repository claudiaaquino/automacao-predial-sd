<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Rules Controller
 *
 * @property \App\Model\Table\RulesTable $Rules
 */
class RulesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $rules = $this->paginate($this->Rules);

        $this->set(compact('rules'));
        $this->set('_serialize', ['rules']);
    }

    /**
     * View method
     *
     * @param string|null $id Rule id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $rule = $this->Rules->get($id, [
            'contain' => []
        ]);

        $this->set('rule', $rule);
        $this->set('_serialize', ['rule']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $rule = $this->Rules->newEntity();
        if ($this->request->is('post')) {
            $rule = $this->Rules->patchEntity($rule, $this->request->data);
            $rule->created =  date('Y-m-d H:i:s');
            $rule->user_id =  $this->Auth->user('id');
            if ($this->Rules->save($rule)) {
                $this->Flash->success(__('Saved successfully'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('An error occured, please try later.'));
            }
        }

        $devices = $this->Rules->Devices->find('list');
        $this->set(compact('rule', 'devices'));
        $this->set('_serialize', ['rule', 'devices']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Rule id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $rule = $this->Rules->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rule = $this->Rules->patchEntity($rule, $this->request->data);
            if ($this->Rules->save($rule)) {
                $this->Flash->success(__('Saved successfully'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('An error occured, please try later.'));
            }
        }
        $devices = $this->Rules->Devices->find('list');
        $this->set(compact('rule', 'devices'));
        $this->set('_serialize', ['rule', 'devices']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Rule id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $rule = $this->Rules->get($id);
        if ($this->Rules->delete($rule)) {
            $this->Flash->success(__('Removed successfully'));
        } else {
            $this->Flash->error(__('An error occured, please try later.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
