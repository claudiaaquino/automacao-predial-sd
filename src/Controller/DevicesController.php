<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Devices Controller
 *
 * @property \App\Model\Table\DevicesTable $Devices
 */
class DevicesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function dashboard()
    {
        $devices = $this->Devices;


        $this->set(compact('devices'));
        $this->set('_serialize', ['devices']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $devices = $this->paginate($this->Devices, ['contain' => ['Nos']]);

        $this->set(compact('devices'));
        $this->set('_serialize', ['devices']);
    }

    /**
     * findAll method
     *
     * @return \Cake\Network\Response|null
     */
    public function all()
    {
        $devices = $this->Devices->find('list');
        $this->set(compact('devices'));
        $this->set('_serialize', ['devices']);
        $this->RequestHandler->renderAs($this, 'json');
    }

    public function realtime($id = null)
    {
        $device = $this->Devices->get($id, [
            'contain' => ['Nos']
        ]);

        $this->set('device', $device);
        $this->set('_serialize', ['device']);
    }

    /**
     * View method
     *
     * @param string|null $id Device id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $device = $this->Devices->get($id, [
            'contain' => ['Nos']
        ]);

        $this->set('device', $device);
        $this->set('_serialize', ['device']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $device = $this->Devices->newEntity();
        if ($this->request->is('post')) {
            $device = $this->Devices->patchEntity($device, $this->request->data);
            $device->created = date('Y-m-d H:i:s');
            $device->user_id = $this->Auth->user('id');
            if ($this->Devices->save($device)) {
                $this->Flash->success(__('Saved successfully'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('An error occured, please try later.'));
            }
        }

        $nos = $this->Devices->Nos->find('list');
        $this->set(compact('device', 'nos'));
        $this->set('_serialize', ['device', 'nos']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Device id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $device = $this->Devices->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $device = $this->Devices->patchEntity($device, $this->request->data);
            if ($this->Devices->save($device)) {
                $this->Flash->success(__('Saved successfully'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('An error occured, please try later.'));
            }
        }

        $nos = $this->Devices->Nos->find('list');
        $this->set(compact('device', 'nos'));
        $this->set('_serialize', ['device', 'nos']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Device id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $device = $this->Devices->get($id);
        if ($this->Devices->delete($device)) {
            $this->Flash->success(__('Removed successfully'));
        } else {
            $this->Flash->error(__('An error occured, please try later.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
