<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Nos Controller
 *
 * @property \App\Model\Table\NosTable $Nos
 */
class NosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $nos = $this->paginate($this->Nos);

        $this->set(compact('nos'));
        $this->set('_serialize', ['nos']);
    }

    /**
     * View method
     *
     * @param string|null $id No id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $no = $this->Nos->get($id, [
            'contain' => []
        ]);

        $this->set('no', $no);
        $this->set('_serialize', ['no']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $no = $this->Nos->newEntity();
        if ($this->request->is('post')) {
            $no = $this->Nos->patchEntity($no, $this->request->data);
            $no->created =  date('Y-m-d H:i:s');
            $no->user_id =  $this->Auth->user('id');
            if ($this->Nos->save($no)) {
                $this->Flash->success(__('Saved successfully'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('An error occured, please try later.'));
            }
        }
        $this->set(compact('no'));
        $this->set('_serialize', ['no']);
    }

    /**
     * Edit method
     *
     * @param string|null $id No id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $no = $this->Nos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $no = $this->Nos->patchEntity($no, $this->request->data);
            if ($this->Nos->save($no)) {
                $this->Flash->success(__('Saved successfully'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('An error occured, please try later.'));
            }
        }
        $this->set(compact('no'));
        $this->set('_serialize', ['no']);
    }

    /**
     * Delete method
     *
     * @param string|null $id No id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $no = $this->Nos->get($id);
        if ($this->Nos->delete($no)) {
            $this->Flash->success(__('Removed successfully'));
        } else {
            $this->Flash->error(__('An error occured, please try later.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
