<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Nos Model
 *
 * @property \Cake\ORM\Association\HasMany $Devices
 *
 * @method \App\Model\Entity\No get($primaryKey, $options = [])
 * @method \App\Model\Entity\No newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\No[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\No|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\No patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\No[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\No findOrCreate($search, callable $callback = null)
 */
class NosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('nos');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->hasMany('Devices', [
            'foreignKey' => 'no_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('name');

        $validator
            ->integer('flagfail')
            ->allowEmpty('flagfail');

        return $validator;
    }
}
