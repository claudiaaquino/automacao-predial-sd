<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NosTable Test Case
 */
class NosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\NosTable
     */
    public $Nos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.nos',
        'app.devices',
        'app.logs',
        'app.rules'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Nos') ? [] : ['className' => 'App\Model\Table\NosTable'];
        $this->Nos = TableRegistry::get('Nos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Nos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
